import Foundation

struct Product: Codable {

    let gtin14: String
    let name: String?
    let author: String?
    let size: String?
    let images: [[String: String]]

    let ingredients: String?
    let format: String?
    let publisher: String?
    let pages: Int?
}
