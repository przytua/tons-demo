import UIKit

protocol DetailsBusinessLogic {

    func fetchProduct()
}

final class DetailsInteractor {
  
    private let presenter: DetailsPresentationLogic

    private var product: Product
  
    init(_ presenter: DetailsPresentationLogic, product: Product) {
        self.presenter = presenter
        self.product = product
    }
}

extension DetailsInteractor: DetailsBusinessLogic {

    func fetchProduct() {
        presenter.present(product: self.product)
    }
}
