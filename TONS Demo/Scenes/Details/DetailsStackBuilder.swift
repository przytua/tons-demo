import UIKit

struct DetailsContext {
    
    let commonContext: CommonContext
    let product: Product
    
    init(commonContext: CommonContext, product: Product) {
        self.commonContext = commonContext
        self.product = product
    }
}

final class DetailsStackBuilder {

    public func setup(viewController: DetailsViewController, with context: DetailsContext) {
        let presenter = DetailsPresenter()
        let interactor = DetailsInteractor(presenter, product: context.product)
        let router = DetailsRouter(context.commonContext)
        viewController.interactor = interactor
        viewController.router = router
        presenter.viewController = viewController
    }
}
