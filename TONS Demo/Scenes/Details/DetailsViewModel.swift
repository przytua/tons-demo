import Foundation

struct DetailsViewModel {

    struct ProductDetail {

        let label: String
        let value: String
    }

    let productDescription: String
    let details: [ProductDetail]

    init(with product: Product) {
        self.productDescription = product.name ?? "No desctiption"
        var details = [ProductDetail]()
        if let size = product.size {
            details.append(ProductDetail(label: "Size", value: size))
        }
        if let ingredients = product.ingredients {
            details.append(ProductDetail(label: "Ingredients", value: ingredients))
        }
        if let author = product.author {
            details.append(ProductDetail(label: "Author", value: author))
        }
        if let format = product.format {
            details.append(ProductDetail(label: "Format", value: format))
        }
        if let publisher = product.publisher {
            details.append(ProductDetail(label: "Publisher", value: publisher))
        }
        if let pages = product.pages {
            details.append(ProductDetail(label: "Pages", value: "\(pages)"))
        }
        self.details = details
    }
}
