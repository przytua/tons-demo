import UIKit

protocol DetailsPresentationLogic {

    func present(product: Product)
}

final class DetailsPresenter {
  
    weak var viewController: DetailsDisplayLogic?    
}

extension DetailsPresenter: DetailsPresentationLogic {

    func present(product: Product) {
        let viewModel = DetailsViewModel(with: product)
        viewController?.display(viewModel: viewModel)
    }
}
