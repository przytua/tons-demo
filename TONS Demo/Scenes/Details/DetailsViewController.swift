import UIKit

protocol DetailsDisplayLogic: class {

    func display(viewModel: DetailsViewModel)
}

final class DetailsViewController: UIViewController {
    
    var router: DetailsRouter!
    var interactor: DetailsBusinessLogic?

    private let detailCellReuseIdentifier = "DetailCell"

    @IBOutlet private weak var detailDescriptionLabel: UILabel!
    @IBOutlet private weak var tableView: UITableView!

    private var viewModel: DetailsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        hideTbaleViewSeparators()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        interactor?.fetchProduct()
    }

    private func hideTbaleViewSeparators() {
        tableView.tableFooterView = UIView()
    }
}

extension DetailsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.details.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: detailCellReuseIdentifier, for: indexPath)
        guard let detail = viewModel?.details[indexPath.row] else {
            return cell
        }
        cell.textLabel?.text = detail.label
        cell.detailTextLabel?.text = detail.value
        return cell
    }
}

extension DetailsViewController: DetailsDisplayLogic {

    func display(viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        detailDescriptionLabel.text = viewModel.productDescription
        tableView.reloadData()
    }
}
