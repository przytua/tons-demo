import XCTest
@testable import TONS_Demo

class ListViewModelTests: XCTestCase {

    func testParsingWithSize() {
        let model = Product(
            gtin14: "test code",
            name: "test name",
            author: nil,
            size: "test size",
            images: [
                ["url" : "http://example.com/image.jpeg"]
            ]
        )
        let viewModel = ListViewModel.Product(with: model)
        XCTAssertEqual(viewModel.name, "test name")
        XCTAssertEqual(viewModel.detail, "Size: test size")
        XCTAssertEqual(viewModel.imageURL, URL(string: "http://example.com/image.jpeg"))
    }

    func testParsingWithAuthor() {
        let model = Product(
            gtin14: "test code",
            name: "test name",
            author: "test author",
            size: nil,
            images: [
                ["url" : "http://example.com/image.jpeg"]
            ]
        )
        let viewModel = ListViewModel.Product(with: model)
        XCTAssertEqual(viewModel.name, "test name")
        XCTAssertEqual(viewModel.detail, "Author: test author")
        XCTAssertEqual(viewModel.imageURL, URL(string: "http://example.com/image.jpeg"))
    }

    func testParsingWithoutSizeAndAuthor() {
        let model = Product(
            gtin14: "test code",
            name: "test name",
            author: nil,
            size: nil,
            images: [
                ["url" : "http://example.com/image.jpeg"]
            ]
        )
        let viewModel = ListViewModel.Product(with: model)
        XCTAssertEqual(viewModel.name, "test name")
        XCTAssertEqual(viewModel.detail, nil)
        XCTAssertEqual(viewModel.imageURL, URL(string: "http://example.com/image.jpeg"))
    }

    func testParsingWithoutName() {
        let model = Product(
            gtin14: "test code",
            name: nil,
            author: nil,
            size: nil,
            images: [
                ["url" : "http://example.com/image.jpeg"]
            ]
        )
        let viewModel = ListViewModel.Product(with: model)
        XCTAssertEqual(viewModel.name, "")
        XCTAssertEqual(viewModel.detail, nil)
        XCTAssertEqual(viewModel.imageURL, URL(string: "http://example.com/image.jpeg"))
    }
}
