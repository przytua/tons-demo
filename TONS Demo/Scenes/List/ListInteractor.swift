import UIKit

protocol ListBusinessLogic {

    func fetchItems()
}

protocol ListDataStore {

    var products: [Product]? { get }
}

final class ListInteractor: ListDataStore {
  
    private let presenter: ListPresentationLogic
    private let worker: ListWorking

    private(set) var products: [Product]?
  
    init(_ presenter: ListPresentationLogic, worker: ListWorking) {
        self.presenter = presenter
        self.worker = worker
    }
}

extension ListInteractor: ListBusinessLogic {

    func fetchItems() {
        worker.fetchItems { [weak self] result in
            switch result {
            case .success(let products):
                self?.products = products
                self?.presenter.present(products: products)
            case .failure(let error):
                print(error)
            }
        }
    }
}
