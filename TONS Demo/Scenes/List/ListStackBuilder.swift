import UIKit

struct ListContext {
    
    let commonContext: CommonContext
    
    init(commonContext: CommonContext) {
        self.commonContext = commonContext
    }
}

final class ListStackBuilder {

    public func setup(viewController: ListViewController, with context: ListContext) {
        let presenter = ListPresenter()
        let worker = ListWorker(datakickClient: context.commonContext.datakickClient)
        let interactor = ListInteractor(presenter, worker: worker)
        let router = ListRouter(context.commonContext, dataStore: interactor)
        viewController.interactor = interactor
        viewController.router = router
        presenter.viewController = viewController
    }
}
