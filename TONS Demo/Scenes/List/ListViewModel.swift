import Foundation

struct ListViewModel {

    struct Product {

        let name: String
        let detail: String?
        let imageURL: URL?

        init(with product: TONS_Demo.Product) {
            self.name = product.name ?? ""
            if let size = product.size {
                self.detail = "Size: \(size)"
            } else if let author = product.author {
                self.detail = "Author: \(author)"
            } else {
                self.detail = nil
            }
            if let imageURL = product.images.first?["url"] {
                self.imageURL = URL(string: imageURL)
            } else {
                self.imageURL = nil
            }
        }
    }

    let products: [Product]
}
