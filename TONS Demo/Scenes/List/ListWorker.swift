import Foundation

protocol ListWorking {

    func fetchItems(completionHandler: @escaping (Result<[Product], Error>) -> Void)
}

final class ListWorker {

    private let datakickClient: DatakickFetching
    private let decoder = JSONDecoder()

    init(datakickClient: DatakickFetching) {
        self.datakickClient = datakickClient
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
}

extension ListWorker: ListWorking {

    func fetchItems(completionHandler: @escaping (Result<[Product], Error>) -> Void) {
        datakickClient.getItems { result in
            switch result {
            case .success(let data):
                do {
                    let products = try JSONDecoder().decode([Product].self, from: data)
                    completionHandler(.success(products))
                } catch {
                    completionHandler(.failure(error))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}
