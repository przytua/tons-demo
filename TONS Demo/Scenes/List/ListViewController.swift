import UIKit
import AlamofireImage

protocol ListDisplayLogic: class {

    func display(viewModel: ListViewModel)
}

final class ListViewController: UITableViewController {

    var interactor: ListBusinessLogic!
    var router: ListRouter!

    private let showDetailSegueIdentifier = "showDetail"
    private let productCellReuseIdentifier = "ProductCell"

    private var viewModel: ListViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.fetchItems()
        let searchController = router.getSearchController()
        navigationItem.searchController = searchController
        navigationController?.definesPresentationContext = true
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController?.isCollapsed ?? false
        super.viewWillAppear(animated)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == showDetailSegueIdentifier,
              let indexPath = tableView.indexPathForSelectedRow else {
            return
        }
        router.routeToDetails(with: segue, itemAt: indexPath.row)
    }

    // MARK: - Table View

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.products.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: productCellReuseIdentifier, for: indexPath)
        guard let product = viewModel?.products[indexPath.row] else {
            return cell
        }
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = product.detail
        if let imageURL = product.imageURL {
            cell.imageView?.af_setImage(withURL: imageURL) { response in
                cell.imageView?.setNeedsLayout()
            }
        }
        return cell
    }
}

extension ListViewController: ListDisplayLogic {

    func display(viewModel: ListViewModel) {
        self.viewModel = viewModel
        tableView.reloadData()
    }
}
