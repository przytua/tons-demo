import UIKit

protocol ListPresentationLogic {

    func present(products: [Product])
}

final class ListPresenter {
  
    weak var viewController: ListDisplayLogic?
}

extension ListPresenter: ListPresentationLogic {

    func present(products: [Product]) {
        let products = products.map { ListViewModel.Product(with: $0) }
        let viewModel = ListViewModel(products: products)
        viewController?.display(viewModel: viewModel)
    }
}
