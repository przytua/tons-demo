import UIKit
import AlamofireImage

class ListCell: UITableViewCell {

    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
        detailTextLabel?.text = nil
        imageView?.af_cancelImageRequest()
        imageView?.image = nil
    }
}
