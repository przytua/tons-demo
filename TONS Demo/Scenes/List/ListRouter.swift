import UIKit

final class ListRouter {
    
    let commonContext: CommonContext
    private let dataStore: ListDataStore
    
    init(_ commonContext: CommonContext, dataStore: ListDataStore) {
        self.commonContext = commonContext
        self.dataStore = dataStore
    }

    func getSearchController() -> UISearchController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "ListViewController") as! ListViewController
        let searchStackBuilder = SearchStackBuilder()
        let searchContext = SearchContext(commonContext: commonContext)
        let searchController = searchStackBuilder.setup(viewController: viewController, with: searchContext)

        return searchController
    }

    func routeToDetails(with segue: UIStoryboardSegue, itemAt index: Int) {
        guard let navigationController = segue.destination as? UINavigationController else { return }
        guard let detailsViewController = navigationController.topViewController as? DetailsViewController else { return }
        guard let product = dataStore.products?[index] else { return }

        let detailsStackBuilder = DetailsStackBuilder()
        let detailsContext = DetailsContext(commonContext: commonContext, product: product)
        detailsStackBuilder.setup(viewController: detailsViewController, with: detailsContext)
    }
}

