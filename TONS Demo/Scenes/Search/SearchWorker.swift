import Foundation

protocol SearchWorking {

    func searchItems(query: String?, completionHandler: @escaping (Result<[Product], Error>) -> Void)
}

final class SearchWorker {

    private let datakickClient: DatakickFetching
    private let decoder = JSONDecoder()

    init(datakickClient: DatakickFetching) {
        self.datakickClient = datakickClient
        decoder.keyDecodingStrategy = .convertFromSnakeCase
    }
}

extension SearchWorker: SearchWorking {

    func searchItems(query: String?, completionHandler: @escaping (Result<[Product], Error>) -> Void) {
        datakickClient.getItems(query: query) { result in
            switch result {
            case .success(let data):
                do {
                    let products = try JSONDecoder().decode([Product].self, from: data)
                    completionHandler(.success(products))
                } catch {
                    completionHandler(.failure(error))
                }
            case .failure(let error):
                completionHandler(.failure(error))
            }
        }
    }
}
