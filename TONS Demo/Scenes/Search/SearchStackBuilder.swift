import UIKit

struct SearchContext {

    let commonContext: CommonContext

    init(commonContext: CommonContext) {
        self.commonContext = commonContext
    }
}

final class SearchStackBuilder {

    public func setup(viewController: ListViewController, with context: SearchContext) -> UISearchController {
        let presenter = ListPresenter()
        let worker = SearchWorker(datakickClient: context.commonContext.datakickClient)
        let interactor = SearchInteractor(presenter, worker: worker)
        let router = ListRouter(context.commonContext, dataStore: interactor)
        viewController.interactor = interactor
        viewController.router = router
        presenter.viewController = viewController

        let searchController = UISearchController(searchResultsController: viewController)
        searchController.searchResultsUpdater = interactor

        return searchController
    }
}
