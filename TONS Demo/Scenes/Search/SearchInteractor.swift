import UIKit

protocol SearchDataStore {

    var products: [Product]? { get }
}

final class SearchInteractor: NSObject, ListDataStore {

    private let presenter: ListPresentationLogic
    private let worker: SearchWorking

    private(set) var products: [Product]?

    init(_ presenter: ListPresentationLogic, worker: SearchWorking) {
        self.presenter = presenter
        self.worker = worker
    }
}

extension SearchInteractor: ListBusinessLogic {

    func fetchItems() {}
}

extension SearchInteractor: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        guard let searchQuery = searchController.searchBar.text, !searchQuery.isEmpty else { return }
        worker.searchItems(query: searchController.searchBar.text) { [weak self] result in
            switch result {
            case .success(let products):
                self?.products = products
                self?.presenter.present(products: products)
            case .failure(let error):
                print(error)
            }
        }
    }
}
