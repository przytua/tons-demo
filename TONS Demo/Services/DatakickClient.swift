import Foundation
import Alamofire

enum DatakickClientErrors: Error {

    case unknownError
}

protocol DatakickFetching {

    func getItems(completionHandler: @escaping (Swift.Result<Data, Error>) -> Void)
    func getItems(query: String?, completionHandler: @escaping (Swift.Result<Data, Error>) -> Void)
}

final class DatakickClient: DatakickFetching {

    let baseURL = "https://www.datakick.org/api/"

    enum Endpoints {
        static let items = "items"
    }

    func getItems(completionHandler: @escaping (Swift.Result<Data, Error>) -> Void) {
        getItems(query: nil, completionHandler: completionHandler)
    }

    func getItems(query: String?, completionHandler: @escaping (Swift.Result<Data, Error>) -> Void) {
        var parameters: Parameters? = nil
        if let query = query, let encodedQuery = query.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            parameters = ["query": encodedQuery as Any]
        }
        Alamofire.request(url(for: Endpoints.items), parameters: parameters, encoding: URLEncoding()).responseJSON { response in
            guard let responseData = response.data else {
                completionHandler(.failure(response.error ?? DatakickClientErrors.unknownError))
                return
            }
            completionHandler(.success(responseData))
        }
    }

    private func url(for endpoint: String) -> String {
        return "\(baseURL)\(endpoint)"
    }
}
