import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let window = window else { return }
        guard let splitViewController = window.rootViewController as? UISplitViewController else { return }
        guard let listNavigationController = splitViewController.viewControllers.first as? UINavigationController else { return }
        guard let listViewController = listNavigationController.topViewController as? ListViewController else { return }
        guard let detailsNavigationController = splitViewController.viewControllers.last as? UINavigationController else { return }

        let commonContext = CommonContext()

        let listViewStackBuilder = ListStackBuilder()
        let listContext = ListContext(commonContext: commonContext)
        listViewStackBuilder.setup(viewController: listViewController, with: listContext)

        detailsNavigationController.topViewController?.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
        detailsNavigationController.topViewController?.navigationItem.leftItemsSupplementBackButton = true
        splitViewController.delegate = self
        splitViewController.preferredDisplayMode = .allVisible;
    }

    // MARK: - Split view

    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController:UIViewController, onto primaryViewController:UIViewController) -> Bool {
        guard let secondaryAsNavController = secondaryViewController as? UINavigationController else { return false }
        guard let topAsDetailController = secondaryAsNavController.topViewController as? DetailsViewController else { return false }
        if topAsDetailController.interactor == nil {
            return true
        }
        return false
    }
}

